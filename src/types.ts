export type Message = string;

export type TextColor = 'cyan' | 'green' | 'blue' | 'white' | 'red' | 'magenta';

export type BgColor = 'bgCyan' | 'bgGreen' | 'bgBlue' | 'bgWhite' | 'bgRed' | 'bgMagenta';

export type Direction = 'NORTH' | 'SOUTH' | 'WEST' | 'EAST' | string;

export interface CoordinateObject {
  x: number;
  y: number;
}

export type Coordinate = CoordinateObject | null;

export interface TableDimension extends CoordinateObject {}

export interface Facing extends CoordinateObject {}

export type Orientation = Record<Direction, CoordinateObject>;

export interface RobotOptions {
  coordinate: Coordinate;
  direction: Direction;
}

export interface RobotStatus {
  isPlaced: boolean;
  coordinate: Coordinate;
  facing: Facing;
}

export interface ErrorMessages {
  placeError: string;
  inputError: string;
}
