import chalk from 'chalk';
import { textSync } from 'figlet';
import { Message, TextColor, BgColor } from './types';
import { MESSAGE } from './configs';

export default class Notification {
  text(message: Message, textColor: TextColor) {
    console.log(chalk[textColor](message));
  }

  textWithBg(message: Message, bgColor: BgColor) {
    console.log(chalk[bgColor](message));
  }

  printError(message: Message) {
    this.textWithBg(message, 'bgRed');
  }

  printInfo(message: Message) {
    this.textWithBg(message, 'bgBlue');
  }

  printHeading() {
    const message = textSync('ROBOT CHALLENGE', {
      font: 'Big Money-nw'
    });

    this.text(message, 'blue');
  }

  printDescription() {
    this.text(MESSAGE.DESCRIPTION, 'magenta');

    this.text(MESSAGE.INSTRUCTION, 'magenta');

    this.text(MESSAGE.TERMINATE, 'cyan');

    this.text(MESSAGE.START, 'green');
  }
}
