import { Coordinate, Facing, RobotOptions, RobotStatus } from './types';
import { ORIENTATION } from './configs';

export default class Robot {
  // Robot coordinate
  protected coordinate: Coordinate = null;

  // Facing orientation (default as NORTH)
  protected facing: Facing = {
    x: 0,
    y: 1
  };

  // Placed status
  protected isPlaced: boolean = false;

  place({ direction, coordinate }: RobotOptions) {
    this.coordinate = coordinate;
    this.facing = ORIENTATION[direction];
    this.isPlaced = true;
  }

  move() {
    if (this.coordinate === null || !this.isPlaced) {
      return;
    }

    const { x, y } = this.facing;

    this.coordinate = {
      x: this.coordinate.x + x,
      y: this.coordinate.y + y
    };
  }

  turnLeft() {
    if (!this.isPlaced) return;

    const { x, y } = this.facing;

    this.facing = {
      x: -y,
      y: x
    };
  }

  turnRight() {
    if (!this.isPlaced) return;

    const { x, y } = this.facing;

    this.facing = {
      x: y,
      y: -x
    };
  }

  status(): RobotStatus {
    return {
      coordinate: this.coordinate,
      isPlaced: this.isPlaced,
      facing: this.facing
    };
  }

  reset() {
    this.coordinate = null;
    this.isPlaced = false;
    this.facing = { x: 0, y: 1 };
  }
}
