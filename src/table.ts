import { CoordinateObject, TableDimension } from './types';

export default class Table {
  public dimension: TableDimension;

  constructor(dimension: TableDimension = { x: 5, y: 5 }) {
    this.dimension = dimension;
  }

  isOnTable({ x, y }: CoordinateObject): boolean {
    return x > -1 && x < this.dimension.x && y > -1 && y < this.dimension.y;
  }
}
