import Table from './table';
import Notification from './notification';
import Robot from './robot';
import { ErrorMessages, Facing } from './types';
import { COMMANDS, FACING_DIRECTIONS, ORIENTATION, ERRORS } from './configs';

export default class Controller {
  public table: Table;

  public notification: Notification;

  public robot: Robot;

  constructor(table: Table, notification: Notification) {
    this.table = table;

    this.notification = notification;

    this.robot = new Robot();
  }

  execute(command: string) {
    if (command.length === 0) return;

    const { placeError, inputError } = this.checkCommandErrors(command);

    if (inputError.length !== 0) {
      this.notification.printError(inputError);

      this.robot.reset();

      return;
    }

    if (placeError.length !== 0) {
      this.notification.printError(placeError);

      return;
    }

    this.runCommand(command);
  }

  runCommand(inputtedCommand: string) {
    const subCommandValues = this.getSubCommandValues(inputtedCommand);

    const command = subCommandValues[0];

    switch (command) {
      case 'PLACE': {
        const x = parseInt(subCommandValues[1], 10);

        const y = parseInt(subCommandValues[2], 10);

        const f = subCommandValues[3];

        this.robot.place({ coordinate: { x, y }, direction: f });

        break;
      }

      case 'MOVE': {
        this.robot.move();

        break;
      }

      case 'LEFT': {
        this.robot.turnLeft();

        break;
      }

      case 'RIGHT': {
        this.robot.turnRight();

        break;
      }

      case 'REPORT': {
        const { coordinate, facing } = this.robot.status();

        if (coordinate === null) return;

        const reportText = `Output: ${coordinate.x},${coordinate.y},${this.getFacingDirection(
          facing
        )}`;

        this.notification.printInfo(reportText);

        break;
      }
    }
  }

  checkCommandErrors(inputtedCommand: string): ErrorMessages {
    let errors: ErrorMessages = {
      placeError: '',
      inputError: ''
    };

    const { isPlaced, coordinate, facing } = this.robot.status();

    const subCommandValues = this.getSubCommandValues(inputtedCommand);

    const command = subCommandValues[0];

    if (command.length === 0) return errors;

    // Error for invalid command
    if (!COMMANDS.includes(command)) {
      errors.inputError = ERRORS.invalidCommand;

      return errors;
    }

    if (command === 'PLACE') {
      // Error for invalid initial command
      if (subCommandValues.length < 4) {
        errors.inputError = ERRORS.invalidInitialCommand;

        return errors;
      } else {
        const x = parseInt(subCommandValues[1], 10);

        const y = parseInt(subCommandValues[2], 10);

        const f = subCommandValues[3];

        if (!this.isValidCoordinate(x) || !this.isValidCoordinate(y)) {
          errors.inputError = ERRORS.wrongCoordinate;

          return errors;
        }

        if (!FACING_DIRECTIONS.includes(f)) {
          errors.inputError = ERRORS.wrongDirection;

          return errors;
        }

        if (!this.table.isOnTable({ x, y })) {
          errors.inputError = ERRORS.wrongPlace;

          return errors;
        }
      }
    }

    // Error for not initializing
    if (command !== 'PLACE' && !isPlaced && coordinate === null) {
      errors.inputError = ERRORS.notInitialized;

      return errors;
    }

    // Error for falling off table
    if (
      command === 'MOVE' &&
      isPlaced &&
      coordinate !== null &&
      !this.table.isOnTable({ x: coordinate.x + facing.x, y: coordinate.y + facing.y })
    ) {
      errors.placeError = ERRORS.wrongMovingDirection;

      return errors;
    }

    return errors;
  }

  getSubCommandValues(command: string): string[] {
    return command.split(/[\s,]+/);
  }

  isValidCoordinate(x: number): boolean {
    return Number.isInteger(x) && Math.sign(x) >= 0;
  }

  getFacingDirection({ x, y }: Facing) {
    const keys = Object.keys(ORIENTATION);

    return keys.find(k => {
      const value = ORIENTATION[k];

      return value.x === x && value.y === y;
    });
  }
}
