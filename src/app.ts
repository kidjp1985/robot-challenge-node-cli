import Controller from './controller';
import Notification from './notification';
import Table from './table';

const stdin = process.openStdin();

const notification = new Notification();

const table = new Table();

const controller = new Controller(table, notification);

// Print heading
notification.printHeading();

// Print game description
notification.printDescription();

// Receive & execute inputted command
stdin.addListener('data', input => {
  let command = input
    .toString()
    .trim()
    .toUpperCase();

  controller.execute(command);
});
