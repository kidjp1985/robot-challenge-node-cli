robot-challenge
===============

> A command-line application built with NodeJS (v10.15.3)

## Description

- The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement
  that would result in the robot falling from the table must be prevented, however further valid movement commands must still
  be allowed.

## Challenge required

Create an application that can read in commands of the following form:

```plain
PLACE X,Y,F
MOVE
LEFT
RIGHT
REPORT
```

- PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.
- The origin (0,0) can be considered to be the SOUTH WEST most corner.
- The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed.
- MOVE will move the toy robot one unit forward in the direction it is currently facing.
- LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot.
- REPORT will announce the X,Y and orientation of the robot.
- A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands.
- Provide test data to exercise the application.

## Expectations

- production quality code
- good OO or functional practices
- a solid testing approach
- well thought out naming
- solid error handling
- extensibility/maintainability/\*ility
- good design
- separation of concerns
- sensible breakdown of code into files/modules
- use of best practices when it comes to JS, CSS, testing etc.
- appropriate use of tools/frameworks

## Things you can find from this **robot-challenge** application

- Solved all [required challenges](#markdown-header-challenge-required) & [expectations](#markdown-header-expectations)
- Fully built with Typescript to provide a strongly-typed code base

## Completed application screenshot

![alt screenshot](https://i.imgur.com/hLbnvig.png)

## Built with

- **[TypeScript](https://www.typescriptlang.org/)**
- **[NodeJs](https://nodejs.org/en/)**

## Get started

#### Make sure you have the latest git, Node.js and Yarn installed on your machine

```bash
$ git --version

$ yarn --version

$ node --version
```

#### Clone the repo from Bitbucket

```bash
$ git clone git@bitbucket.org:kidjp1985/robot-challenge-node-cli.git
```

#### Install npm package

```bash
$ cd robot-challenge-node-cli && yarn
```

#### Running from source

```bash
$ yarn start
```

#### Testing

```bash
$ yarn test
```

![alt screenshot](https://i.imgur.com/nSMQSkY.png)

