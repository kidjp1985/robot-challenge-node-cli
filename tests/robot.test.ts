import Robot from '../src/robot';

describe('Robot', () => {
  describe('#initialize', () => {
    test('it should be initialized with the given params', () => {
      const robot = new Robot();

      expect(robot.status()).toEqual({
        coordinate: null,
        isPlaced: false,
        facing: { x: 0, y: 1 }
      });
    });
  });

  describe('#place', () => {
    test('it should update status when robot is placed', () => {
      const robot = new Robot();

      robot.place({ coordinate: { x: 3, y: 4 }, direction: 'EAST' });

      expect(robot.status()).toEqual({
        coordinate: { x: 3, y: 4 },
        isPlaced: true,
        facing: { x: 1, y: 0 }
      });
    });

    test('it should not execute any command except PLACE when robot is not placed', () => {
      const robot = new Robot();

      robot.move();

      robot.turnRight();

      robot.turnLeft();

      expect(robot.status()).toEqual({
        coordinate: null,
        isPlaced: false,
        facing: { x: 0, y: 1 }
      });
    });
  });

  describe('#move', () => {
    const robot = new Robot();

    test('it should move when robot is not placed', () => {
      robot.place({ coordinate: { x: 3, y: 4 }, direction: 'EAST' });

      robot.move();

      expect(robot.status()).toEqual({
        coordinate: { x: 4, y: 4 },
        isPlaced: true,
        facing: { x: 1, y: 0 }
      });
    });
  });

  describe('#turnLeft', () => {
    const robot = new Robot();

    test('it should turn left when robot is  placed', () => {
      robot.place({ coordinate: { x: 3, y: 4 }, direction: 'EAST' });

      robot.turnLeft();

      expect(robot.status()).toEqual({
        coordinate: { x: 3, y: 4 },
        isPlaced: true,
        facing: { x: -0, y: 1 }
      });
    });
  });

  describe('#turnRight', () => {
    const robot = new Robot();

    test('it should turn right when robot is placed', () => {
      robot.place({ coordinate: { x: 3, y: 4 }, direction: 'EAST' });

      robot.turnRight();

      expect(robot.status()).toEqual({
        coordinate: { x: 3, y: 4 },
        isPlaced: true,
        facing: { x: 0, y: -1 }
      });
    });
  });

  describe('#status', () => {
    const robot = new Robot();

    test('it should return robot current status', () => {
      robot.place({ coordinate: { x: 1, y: 2 }, direction: 'EAST' });
      robot.move();
      robot.move();
      robot.turnLeft();
      robot.move();

      expect(robot.status()).toEqual({
        coordinate: { x: 3, y: 3 },
        isPlaced: true,
        facing: { x: -0, y: 1 }
      });
    });
  });

  describe('#reset', () => {
    const robot = new Robot();

    robot.place({ coordinate: { x: 0, y: 0 }, direction: 'NORTH' });
    robot.turnLeft();

    test('it should reset robot initial status', () => {
      expect(robot.status()).toEqual({
        coordinate: { x: 0, y: 0 },
        isPlaced: true,
        facing: { x: -1, y: 0 }
      });

      robot.reset();

      expect(robot.status()).toEqual({
        coordinate: null,
        isPlaced: false,
        facing: { x: 0, y: 1 }
      });
    });
  });
});
