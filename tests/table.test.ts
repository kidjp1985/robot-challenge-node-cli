import Table from '../src/table';

describe('Table', () => {
  describe('#initialize', () => {
    test('it should be initialized with the default params', () => {
      const robot = new Table();

      const { dimension } = robot;

      expect(dimension.x).toEqual(5);

      expect(dimension.y).toEqual(5);
    });

    test('it should be initialized with the custom params', () => {
      const robot = new Table({ x: 6, y: 8 });

      const { dimension } = robot;

      expect(dimension.x).toEqual(6);

      expect(dimension.y).toEqual(8);
    });
  });

  describe('#isOnTable', () => {
    const robot = new Table();

    test('it should return false when robot is not placed on table', () => {
      expect(robot.isOnTable({ x: 4, y: 5 })).toBeFalsy();
    });

    test('it should return true when robot is placed on table', () => {
      expect(robot.isOnTable({ x: 4, y: 2 })).toBeTruthy();
    });
  });
});
