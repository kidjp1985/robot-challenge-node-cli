import chalk from 'chalk';
import figlet from 'figlet';
import Notification from '../src/notification';
import { ERRORS, MESSAGE } from '../src/configs';

describe('Notification', () => {
  const notification = new Notification();

  const mockConsole = jest.spyOn(console, 'log');

  beforeEach(() => {
    mockConsole.mockClear();
  });

  describe('#text', () => {
    test('it should print out text with color', () => {
      notification.text('Hello world', 'blue');

      expect(mockConsole).toHaveBeenCalledWith(chalk.blue('Hello world'));
    });
  });

  describe('#textWithBg', () => {
    test('it should print out text with background color', () => {
      notification.textWithBg('Hello world', 'bgCyan');

      expect(mockConsole).toHaveBeenCalledWith(chalk.bgCyan('Hello world'));
    });
  });

  describe('#printError', () => {
    test('it should print out error message with background color red', () => {
      notification.printError(ERRORS.invalidCommand);

      expect(mockConsole).toHaveBeenCalledWith(chalk.bgRed(ERRORS.invalidCommand));
    });
  });

  describe('#printInfo', () => {
    test('it should print out info message with background color blue', () => {
      notification.printInfo('Hello world');

      expect(mockConsole).toHaveBeenCalledWith(chalk.bgBlue('Hello world'));
    });
  });

  describe('#printHeading', () => {
    test('it should print out heading with text color blue', () => {
      const mockTextSync = jest.spyOn(figlet, 'textSync').mockReturnValueOnce('Hello world');

      notification.printHeading();

      expect(mockConsole).toHaveBeenCalledWith(chalk.blue('Hello world'));

      mockTextSync.mockClear();
    });
  });

  describe('#printDescription', () => {
    test('it should print out description with text color', () => {
      notification.printDescription();

      expect(mockConsole).toHaveBeenCalledWith(chalk.magenta(MESSAGE.DESCRIPTION));

      expect(mockConsole).toHaveBeenCalledWith(chalk.magenta(MESSAGE.INSTRUCTION));

      expect(mockConsole).toHaveBeenCalledWith(chalk.cyan(MESSAGE.TERMINATE));

      expect(mockConsole).toHaveBeenCalledWith(chalk.green(MESSAGE.START));
    });
  });
});
