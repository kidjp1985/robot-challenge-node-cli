import Controller from '../src/controller';
import Table from '../src/table';
import Notification from '../src/notification';
import { ERRORS } from '../src/configs';

describe('Controller', () => {
  const table = new Table();

  const notification = new Notification();

  describe('#initialize', () => {
    const controller = new Controller(table, notification);

    test('it should be initialized with the given params', () => {
      expect(controller.table.dimension).toEqual({ x: 5, y: 5 });
    });
  });

  describe('#execute', () => {
    describe('when command is empty', () => {
      const controller = new Controller(table, notification);

      test('it should return when inputted command is empty', () => {
        const mockCheckCommandErrors = jest
          .spyOn(controller, 'checkCommandErrors')
          .mockReturnThis();

        controller.execute('');

        expect(mockCheckCommandErrors).not.toBeCalled();

        mockCheckCommandErrors.mockClear();
      });
    });

    describe('when command is not empty', () => {
      const controller = new Controller(table, notification);

      test('it should raise input error', () => {
        const mockPrintError = jest.spyOn(controller.notification, 'printError');

        controller.execute('INVALID COMMAND');

        expect(mockPrintError).toBeCalledWith(ERRORS.invalidCommand);

        mockPrintError.mockClear();
      });

      test('it should raise place error', () => {
        const mockPrintError = jest.spyOn(controller.notification, 'printError');

        controller.robot.place({ coordinate: { x: 0, y: 0 }, direction: 'SOUTH' });

        controller.execute('MOVE');

        expect(mockPrintError).toBeCalledWith(ERRORS.wrongMovingDirection);

        controller.robot.reset();

        mockPrintError.mockClear();
      });

      test('it should run command if there is no error', () => {
        const mockRunCommand = jest.spyOn(controller, 'runCommand');

        controller.execute('PLACE 1,1,NORTH');

        expect(mockRunCommand).toBeCalledWith('PLACE 1,1,NORTH');

        mockRunCommand.mockClear();
      });
    });
  });

  describe('#runCommand', () => {
    const controller = new Controller(table, notification);

    describe('PLACE', () => {
      test('it should place the robot', () => {
        const mockPlace = jest.spyOn(controller.robot, 'place');

        controller.runCommand('PLACE 1,1,NORTH');

        expect(mockPlace).toBeCalledWith({ coordinate: { x: 1, y: 1 }, direction: 'NORTH' });

        mockPlace.mockClear();
      });
    });

    describe('MOVE', () => {
      test('it should move the robot', () => {
        const mockMove = jest.spyOn(controller.robot, 'move');

        controller.runCommand('MOVE');

        expect(mockMove).toBeCalledTimes(1);

        mockMove.mockClear();
      });
    });

    describe('LEFT', () => {
      test('it should turn left the robot', () => {
        const mockTurnLeft = jest.spyOn(controller.robot, 'turnLeft');

        controller.runCommand('LEFT');

        expect(mockTurnLeft).toBeCalledTimes(1);

        mockTurnLeft.mockClear();
      });
    });

    describe('RIGHT', () => {
      test('it should turn right the robot', () => {
        const mockTurnRight = jest.spyOn(controller.robot, 'turnRight');

        controller.runCommand('RIGHT');

        expect(mockTurnRight).toBeCalledTimes(1);

        mockTurnRight.mockClear();
      });
    });

    describe('REPORT', () => {
      test('it should report current position', () => {
        const mockPrintInfo = jest.spyOn(controller.notification, 'printInfo');

        controller.runCommand('REPORT');

        expect(mockPrintInfo).toBeCalledWith('Output: 1,2,NORTH');

        mockPrintInfo.mockClear();
      });

      test('it should not report current position when robot is not placed', () => {
        const mockPrintInfo = jest.spyOn(controller.notification, 'printInfo');

        controller.robot.reset();

        controller.runCommand('REPORT');

        expect(mockPrintInfo).not.toBeCalled();

        mockPrintInfo.mockClear();
      });
    });
  });

  describe('#checkCommandErrors', () => {
    describe('when robot is not placed', () => {
      const controller = new Controller(table, notification);

      test('it should return no error message when command is empty', () => {
        expect(controller.checkCommandErrors('')).toEqual({
          placeError: '',
          inputError: ''
        });
      });

      test('it should return invalid command error', () => {
        expect(controller.checkCommandErrors('INVALID COMMAND').inputError).toEqual(
          ERRORS.invalidCommand
        );
      });

      test('it should return invalid PLACE command error', () => {
        expect(controller.checkCommandErrors('PLACE').inputError).toEqual(
          ERRORS.invalidInitialCommand
        );
      });

      test('it should return invalid coordinate value error', () => {
        expect(controller.checkCommandErrors('PLACE -1,A,NORTH').inputError).toEqual(
          ERRORS.wrongCoordinate
        );
      });

      test('it should return invalid facing value error', () => {
        expect(controller.checkCommandErrors('PLACE 1,1,N').inputError).toEqual(
          ERRORS.wrongDirection
        );
      });

      test('it should return placing error', () => {
        expect(controller.checkCommandErrors('PLACE 4,6,NORTH').inputError).toEqual(
          ERRORS.wrongPlace
        );
      });

      test('it should return not initializing error', () => {
        expect(controller.checkCommandErrors('LEFT').inputError).toEqual(ERRORS.notInitialized);
      });
    });

    describe('when robot is placed', () => {
      const controller = new Controller(table, notification);

      test('it should return wrong moving direction error', () => {
        controller.robot.place({ coordinate: { x: 0, y: 0 }, direction: 'SOUTH' });

        expect(controller.checkCommandErrors('MOVE').placeError).toEqual(
          ERRORS.wrongMovingDirection
        );
      });
    });
  });

  describe('#getSubCommandValues', () => {
    const controller = new Controller(table, notification);

    test('it should return array of subcommand value', () => {
      expect(controller.getSubCommandValues('PLACE 1,1,NORTH')).toEqual([
        'PLACE',
        '1',
        '1',
        'NORTH'
      ]);
    });
  });

  describe('#isValidCoordinate', () => {
    const controller = new Controller(table, notification);

    test('it should return false with invalid coordinate value', () => {
      expect(controller.isValidCoordinate(-1)).toBeFalsy();
      expect(controller.isValidCoordinate(12.5)).toBeFalsy();
    });

    test('it should return true with valie coordinate value', () => {
      expect(controller.isValidCoordinate(4)).toBeTruthy();
    });
  });

  describe('#getFacingDirection', () => {
    const controller = new Controller(table, notification);

    test('it should return facing direction value', () => {
      expect(controller.getFacingDirection({ x: 0, y: 1 })).toEqual('NORTH');
      expect(controller.getFacingDirection({ x: 1, y: 0 })).toEqual('EAST');
      expect(controller.getFacingDirection({ x: 0, y: -1 })).toEqual('SOUTH');
      expect(controller.getFacingDirection({ x: -1, y: 0 })).toEqual('WEST');
    });
  });
});
